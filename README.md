# Senior Angular Developer Challenge
### Introduction
Everyday we are challenged with new features that we need to add to our platform, ranges from customized UI blocks that performs specific functionality to enhancing and customizing the currently existing libraries that we already use.

### Goal
The goal of this challenge is to create Files Space. A dropzone and file select component that allows the user to select/drop files from his local machine to a web form. The File Space component show list all the selected files like the following table:-

| # | File Type | File Name | File Size | File Status | Actions |
|--|--|--|--|--|--|
| 1 | PDF | My CV | 1.2MB | 0/1.2MB | Delete - Upload |
| 1 | PNG | Logo | 427KB | 0/427KB | Delete - Upload |

The table will list all the files selected or dropped into the dropzone. The component will report the upload status of each file and update the UI correspondingly.  The component should handle uploading files at specific file size (limit the selected file size, if it exceeds the user should be notified and will not be able to upload this file), also the component should alert the user when a file has completed upload. Use any API endpoint that allows file upload like [Google Drive](https://developers.google.com/drive/api/v3/manage-uploads)

### Criteria of Acceptance
* Well organized and documented code.
* Creating a service for handling API requests.
* Create a reusable and customizable component with proper Input and Methods that can fit any project.
* Don't use any third party libraries for handling file selection, only native File APIs

### Bonus
* Use Angular Material to implement your UI.
* Use Material Progress Spinner to display the file uploading status.
* Use Material Snackbar to show alerts.
* Use SCSS. 
* Create a [StackBlitz](https://stackblitz.com/) demo.